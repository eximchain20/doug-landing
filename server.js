var express = require('express')
var app = express()
var path = require('path')

//handleDisconnect();
var bodyParser = require('body-parser');
app.use('/', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


app.set('port', (process.env.PORT || 8080))
// serve from proxy
//app.use(express.static(__dirname + '/public'))

// Additional middleware which will set headers that we need on each request.
app.use(function(req, res, next) {
    // Set permissive CORS header - this allows this server to be used only as
    // an API server in conjunction with something like webpack-dev-server.
    res.setHeader('Access-Control-Allow-Origin', '*');

    //res.setHeader('Cache-Control', 'no-cache');
    next();
});


app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
